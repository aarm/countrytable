import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {CountrydataComponent} from './countrydata/countrydata.component';
import {CountryDetailsComponent} from './country-details/country-details.component';

const routes: Routes = [
  { path: '', component: CountrydataComponent },
  { path: 'country/:num', component: CountryDetailsComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
