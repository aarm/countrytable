import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-countrydata',
  templateUrl: './countrydata.component.html',
  styleUrls: ['./countrydata.component.css']
})
export class CountrydataComponent implements OnInit {
  columnDefs = [
    {
      headerName: 'Flag', field: 'flag', cellRenderer: this.abc, width: 265 },
    {headerName: 'Country Name', field: 'name', width: 265},
    {headerName: 'Capital', field: 'capital', width: 265},
    {headerName: 'Details', field: 'alpha3Code', cellRenderer: this.xyz, width: 265}
    ];
  rowData;
  post;
   searchinput = '';
  abc(params) {

    return `<img  src = ${params.value} style="width: 50px; height: 30px"/>`;
  }

  xyz(params) {
    return `<button  type="button"><a  href = country/${params.data.alpha3Code}>Details</a></button>`;
  }

  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    fetch('https://restcountries.eu/rest/v2/all')
      .then(result => result.json())
      .then(rowData => this.rowData = rowData);
  }
 search() {
    const filterdata = [];
    this.dataService.getdata().subscribe(data => {
      this.post = data;

    this.post.forEach(item => {
      if (item.name.toLowerCase().includes(this.searchinput.toLowerCase())) {
        filterdata.push(item);
      }
      this.rowData = filterdata;
    });
    });
    }



}
