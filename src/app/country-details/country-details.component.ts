import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../data.service';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css']
})
export class CountryDetailsComponent implements OnInit {
   private number: any;

  post;
  countryDetail;
  bor: any;
  bod;


  br = [];
  lan = [];
  constructor(private route: ActivatedRoute,
              private dataService: DataService) { }

  ngOnInit(): void {
    this.number = this.route.snapshot.paramMap.get('num');
    this.getdata1();
  }
  private getdata1() {

    this.dataService.getdata().subscribe(data => {
      this.post = data;
      const countryCode = this.number;
      for (let i = 0; i < this.post.length; i++) {
        if (this.post[i].alpha3Code === countryCode) {
          console.log(this.post[i]);
          this.countryDetail = this.post[i];
          this.lan = this.countryDetail.languages;
        }
      }
      this.bod = this.countryDetail.borders;
      const border = this.countryDetail.borders;
      this.br = this.post.filter(function(a) {
        return border.indexOf(a.alpha3Code) >= 0;
      });
    });
  }
  manipulateTime(timezone) {
    const timez = timezone.replace('UTC','').replace(':','.');
    const intTimeZone = parseInt(timez.toString());
    const d = new Date();
    const localTime = d.getTime();
    const localOffset = d.getTimezoneOffset() * 60000;
    const utc = localTime + localOffset;
    const newCalTime = utc + (3600000 * intTimeZone);
    return new Date(newCalTime).toLocaleTimeString();
  }

}
